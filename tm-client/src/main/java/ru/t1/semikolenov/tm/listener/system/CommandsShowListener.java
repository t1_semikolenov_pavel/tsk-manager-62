package ru.t1.semikolenov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.event.ConsoleEvent;
import ru.t1.semikolenov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandsShowListener extends AbstractSystemListCommandListener {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@commandsListener.getName() == #event.name || @commandsListener.getArgument() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final AbstractListener command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}