package ru.t1.semikolenov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.dto.request.DataXmlSaveJaxbRequest;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveJaxbListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveJaxbListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxbRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}