package ru.t1.semikolenov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.event.ConsoleEvent;
import ru.t1.semikolenov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class HelpListener extends AbstractSystemListCommandListener {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show terminal commands.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@helpListener.getName() == #event.name || @helpListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final AbstractListener command : commands)
            System.out.println(command);
    }

}