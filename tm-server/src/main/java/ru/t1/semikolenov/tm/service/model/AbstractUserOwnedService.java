package ru.t1.semikolenov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.service.model.IUserOwnedService;
import ru.t1.semikolenov.tm.comparator.CreatedComparator;
import ru.t1.semikolenov.tm.comparator.DateBeginComparator;
import ru.t1.semikolenov.tm.comparator.StatusComparator;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.field.EmptyDescriptionException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyStatusException;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;
import ru.t1.semikolenov.tm.repository.model.AbstractUserOwnedRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel> extends AbstractService<M>
        implements IUserOwnedService<M> {

    @NotNull
    @Override
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId, getSortType(comparator));
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final ru.t1.semikolenov.tm.enumerated.Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }

    @NotNull
    protected Sort getSortType(@NotNull final Comparator comparator) {
        @NotNull final String fieldName;
        if (comparator == CreatedComparator.INSTANCE) fieldName =  "created";
        else if (comparator == StatusComparator.INSTANCE) fieldName = "status";
        else if (comparator == DateBeginComparator.INSTANCE) fieldName =  "dateBegin";
        else fieldName =  "name";
        return Sort.by(Sort.Direction.ASC, fieldName);
    }

}
