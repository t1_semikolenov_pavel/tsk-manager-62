package ru.t1.semikolenov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserDtoOwnedRepository<TaskDTO> {

    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Transactional
    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
