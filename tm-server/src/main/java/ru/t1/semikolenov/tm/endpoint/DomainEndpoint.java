package ru.t1.semikolenov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.semikolenov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.semikolenov.tm.api.service.IDomainService;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.*;
import ru.t1.semikolenov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.semikolenov.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
       getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxbResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxb();
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxbResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxb();
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxbResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxb();
        return new DataXmlLoadJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxbResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxb();
        return new DataXmlSaveJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}